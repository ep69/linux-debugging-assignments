# user cannot login (disavle password, bad shell)

USER="bob"
PASS="linux"

task() {
    echo "User $USER (password '$PASS') can not login to the system remotely:"
    echo " ssh $USER@localhost"
}

setup() {
    useradd -m $USER
    echo $PASS |passwd --stdin $USER
    usermod --shell /sbin/nologin $USER
    usermod --lock $USER
    mkdir -p $HOME/.ssh && chmod 700 $HOME/.ssh
    ssh-keyscan localhost >>$HOME/.ssh/known_hosts
}

cleanup() {
    userdel -rf $USER
}
