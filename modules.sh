# something with wrong arguments

MODNAME="foo"
FAKENAME="intel"

task() {
    echo "Insert module $MODNAME to running kernel with its parameter set to 1 and make sure everything is working by reading /proc/$MODNAME"
}

setup() {
    rpm -q kernel-devel 2>&1 >/dev/null || yum install -y kernel-devel
    cat >$MODNAME.c <<_EOF
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#define BUFSIZE  100


MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Stanislav Zidek");

static int enabled=0;
module_param(enabled,int,0660);

static struct proc_dir_entry *ent;

static ssize_t mywrite(struct file *file, const char __user *ubuf, size_t count, loff_t *ppos)
{
    printk(KERN_ALERT "Module $MODNAME: write not supported\n");
    return -1;
}

static ssize_t myread(struct file *file, char __user *ubuf,size_t count, loff_t *ppos)
{
    char buf[BUFSIZE];
    int len=0;
    if(*ppos > 0 || count < BUFSIZE)
        return 0;
    if (!enabled) {
        printk(KERN_ALERT "Module $MODNAME: ERROR, not enabled\n");
        len += sprintf(buf,"Module $MODNAME: ERROR, not enabled\n");
    } else {
        len += sprintf(buf,"Module $MODNAME: OK, enabled = %d\n", enabled);
    }

    if(copy_to_user(ubuf,buf,len))
        return -EFAULT;
    *ppos = len;

    return len;
}

static struct file_operations myops =
{
    .owner = THIS_MODULE,
    .read = myread,
    .write = mywrite,
};

static int ${MODNAME}_init(void)
{
    ent=proc_create("$MODNAME", 0660, NULL, &myops);
    printk(KERN_ALERT "Module $MODNAME: loaded\n");

    return 0;
}

static void ${MODNAME}_cleanup(void)
{
    proc_remove(ent);
    printk(KERN_WARNING "Module $MODNAME: cleanup\n");
}

module_init(${MODNAME}_init);
module_exit(${MODNAME}_cleanup);
_EOF

    cat >Makefile <<'_EOF'
obj-m += MODNAME.o
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules_install
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
_EOF
    sed -i "s/MODNAME/$MODNAME/" Makefile
    make # compile and install module

    # blacklist
    #echo -e "blacklist $MODNAME\ninstall $MODNAME /bin/false" >/etc/modprobe.d/$FAKENAME.conf
    cat >/etc/modprobe.d/$FAKENAME.conf <<_EOF
blacklist $MODNAME
install $MODNAME /bin/false
_EOF
}

cleanup() {
    rm -f /etc/modprobe.d/$MODNAME.conf /etc/modprobe.d/$FAKENAME.conf
    find /usr/lib -name $MODNAME.ko -exec rm -f {} \;
    lsmod |grep -q "^$MODNAME[[:space:]]" && modprobe -r $MODNAME
}
