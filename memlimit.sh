
USER="bob"
PASS="linux"
APP="/usr/bin/app"
SIZE=256 #MB

task() {
    echo "User $USER is not able to run $APP due to insufficient memory"
    echo "  su - $USER -c $APP"
}

setup() {
    rpm -q --quiet gcc || yum install -y gcc

    app_c="${APP##*/}.c"
    cat >$app_c <<_EOF
#include <stdio.h>
#include <stdlib.h>

int main()
{
    void *mem;
    size_t size;

    size = $SIZE * 1024 * 1024;
    mem = malloc(size);
    if (!mem) {
        fprintf(stderr, "error: not enough memory, $SIZE MB needed\n");
        exit(-1);
    }

    printf("success\n");
    return 0;
}
_EOF

    gcc $app_c
    rm -f $app_c
    \cp a.out $APP

    useradd -m $USER
    echo $PASS |passwd --stdin $USER >/dev/null
    echo "bob hard as 200000" >/etc/security/limits.d/99-memlimits.conf
}

cleanup() {
    userdel -fr $USER
    rm -f /etc/security/limits.d/99-memlimits.conf
}
