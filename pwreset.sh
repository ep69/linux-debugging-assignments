# cannot call passwd, but can modify /etc/{passwd,shadow} - change root password

USER="bob"
PASS="linux"

task() {
    echo "Let's consider that a Linux file system was mounted under /mnt/broken_system/ with unknown passwords"
    echo "The task is to change the root password there to a known one (e.g. 'linux'), so it is possible boot and login to the system"
    echo "As an evidence, chroot to /mnt/broken_system, run 'su - admin' and 'su - root', where the password will be asked."
}

setup() {
    if [[ -d /mnt/broken_system/ ]]; then
        echo "The /mnt/broken_system/ directory already exists. Quit."
        return 1
    fi
    mkdir /mnt/broken_system/
    dnf -y install --installroot=/mnt/broken_system/ bash vim util-linux
    chmod 666 /mnt/broken_system/dev/null
    chroot /mnt/broken_system/ useradd admin
}

cleanup() {
    rm -rf /mnt/broken_system/
}

