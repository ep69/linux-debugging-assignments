#!/bin/bash

help() {
    echo "The script $0 prepare or clean lab environments"
    echo "Usage: $0 <COMMAND> <LAB>"
    echo "  COMMAND:"
    echo "    list          - lists the available labs"
    echo "    task <LAB>    - List the task of the lab"
    echo "    setup <LAB>   - Set up the lab environment"
    echo "    clean <LAB>   - Cleans the lab environment"
}

list_labs() {
    echo "The available labs are:"
    for file in $(find ./ -maxdepth 1 -type f -name \*.sh ! -path $0); do
        if grep -q setup $file && grep -q cleanup $file && grep -q task $file; then
            echo "  $(basename -s .sh $file)"
        fi
    done
}

load_lab() {
    local lab=$1
    if [[ -f $lab ]] && grep -q setup $lab && grep -q cleanup $lab && grep -q task $lab; then
        . $lab
        return 0
    elif [[ -f $lab.sh ]] && grep -q setup $lab.sh && grep -q cleanup $lab.sh && grep -q task $lab.sh; then
        . $lab.sh
        return 0
    else
        echo "Error: not a valid lab"
        help
        exit 2
    fi
}

if [[ $# -eq 0 ]] || [[ $# -gt 2 ]] ; then
    help
    exit 1
elif [[ $1 == 'list' ]]; then
    list_labs
    exit 0
elif [[ $1 == 'task' ]]; then
    load_lab $2
    task && exit 0
elif [[ $1 == 'setup' ]]; then
    load_lab $2
    setup && exit 0
elif [[ $1 == 'clean' || $1 == 'cleanup' ]]; then
    load_lab $2
    cleanup && exit 0
else
    help
    exit 1
fi
exit 99
