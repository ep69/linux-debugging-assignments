# Problems when mounting a file system

task() {
    echo "Figure out why the test file system can not be mounted and fix the problem."
    echo "The test file system location will be provided when the lab is set up."
}

# Internal stuff

TMP_NAME=$(basename -s .sh $1)

_get_loop_file() {
    LOOP_FILE=$(losetup -j $1 |awk '{print $1}' |cut -d: -f1)
    [[ $LOOP_FILE ]] && ls -l $LOOP_FILE
}

# Setup and cleanup

setup() {
    if ls /tmp/$TMP_NAME.vol.* &>/dev/null; then
        echo "It looks like the file system was already set up."
        echo "Quit."
        return 1
    fi
    IMG_FILE=$(mktemp /tmp/$TMP_NAME.vol.XXXXXX)
    dd if=/dev/zero of=$IMG_FILE bs=1024 count=10000
    ls -l $IMG_FILE
    mkdir /mnt/$TMP_NAME
    losetup -f $IMG_FILE
    _get_loop_file $IMG_FILE
    mkfs.ext4 $LOOP_FILE
    mount $LOOP_FILE /mnt/$TMP_NAME
    fdisk -l $LOOP_FILE
    for dir in A B C D E F; do
        mkdir /mnt/$TMP_NAME/$dir
        for file in a b c d e f; do
            echo "test file">/mnt/$TMP_NAME/$dir/$file
        done
    done
    umount /mnt/$TMP_NAME
    dd if=/dev/zero count=1 bs=4096 seek=0 of=$LOOP_FILE
    echo; echo
    echo "TASK: Try to mount $LOOP_FILE to /mnt/$TMP_NAME"; echo
    #mount $LOOP_FILE /mnt/$TMP_NAME
}

cleanup() {
    mount |grep $TMP_NAME && umount /mnt/$TMP_NAME
    [[ -d /mnt/$TMP_NAME/ ]] && rmdir /mnt/$TMP_NAME
    if ls /tmp/$TMP_NAME.vol.* 2>/dev/null; then
        for i in $( ls /tmp/$TMP_NAME.vol.*); do
            _get_loop_file $i
            [[ $LOOP_FILE ]] && losetup -d $LOOP_FILE
            rm -f $LOOP_FILE
        done
    fi
    losetup -l
    rm -f /tmp/$TMP_NAME.*
}
