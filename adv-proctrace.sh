#!/bin/bash

task() {
    echo "Advanced process tracing: something is writing to /tmp/badfile every 5 seconds. Find out which process."
}

setup() {
    yum install -y /usr/bin/killall
    cat >/usr/bin/covert <<_EOF
#!/bin/bash
while sleep 5; do
    echo "bash script1.sh; rm -f script1.sh" > script2.sh
    echo "bash script0.sh; rm -f script0.sh" > script1.sh
    echo "date >/tmp/badfile" > script0.sh

    bash script2.sh
    rm -f script2.sh
done
_EOF
    chmod +x /usr/bin/covert
    nohup /usr/bin/covert >nohup.out 2>nohup.err </dev/null &
}

cleanup() {
    killall -9 covert
    rm -f /usr/bin/covert
}

