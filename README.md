# Linux debugging assignments

This project aims to implemet simple debugging assignments in 
power user / administrator / dev-ops area. Original motivation is to use them
in job interviews to demonstrate candidate's practical skills.

Assignments are implemented and tested on recent RHEL-7 linux, therefore
should work in CentOS without problems. Compatibility with other distros is
currently unknown.

## Usage
1.  Get a machine which could be thrown-away and ssh there.
2.  Make sure `git` is installed: `rpm -q git || yum install -y git`
3.  Clone the repo: \
  `git clone https://gitlab.com/ep69/linux-debugging-assignments.git
  && cd linux-debugging-assignments/`
4.  See available tasks: \
  `./lab.sh list`
5.  Set up selected task: \
  `./lab.sh setup userlogin`
6.  See the instructions for particular task: \
  `./lab.sh task userlogin`
7. Try to accomplish the task / hand over to candidate / ...
8. Clean up: \
  `./lab.sh cleanup userlogin` or just throw away the whole machine

**Alternative usage:** Just `source userlogin.sh` and issue `setup`, `task`,
and `cleanup` directly.
